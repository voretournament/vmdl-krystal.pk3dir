This repository contains the Krystal player model for Vore Tournament. Note that this model is an external addon, and does not represent a default component of the Vore Tournament project.

Instructions: After installing Xonotic and the Vore Tournament mod, clone this repository inside your data_voretournament directory.