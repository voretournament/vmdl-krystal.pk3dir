textures/player/krystal_body
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_body.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_body_suit
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_body_suit.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_hair
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 10 sin 0 0.05 0 1
 	{
		map textures/player/krystal_hair.tga
		rgbgen lightingDiffuse
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/player/krystal_fluff
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_fluff.tga
		rgbgen lightingDiffuse
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

textures/player/krystal_eyes
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_eyes.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_mouth
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_mouth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_teeth
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_teeth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_glass
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_glass.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_metal_white
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_metal_white.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_metal_black
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_metal_black.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_metal_gold
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_metal_gold.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_armor_body
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_armor_body.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_armor_staff
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_armor_staff.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_suit_boots
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_suit_boots.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_suit_tailbinds
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_suit_tailbinds.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_internal_mouth
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 50 sin -0.1 0.2 0 0.5
 	{
		map textures/player/krystal_mouth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_internal_teeth
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/krystal_teeth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/krystal_internal_stomach
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 100 sin -0.5 1.0 0 0.5
 	{
		map textures/player/krystal_stomach.tga
		rgbgen lightingDiffuse
	}
}
